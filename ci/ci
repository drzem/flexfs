#!/bin/bash 

set -e

BASE_IMAGE="danfoster/flexfs_ci:latest"
DOCKER=docker

PROJECT_DIR=$(git rev-parse --show-toplevel)

function usage {
    echo "USAGE: $0 subcommand [options]"
    echo ""
    echo "Valid subcommands:"
    echo "  lint - Lints"
    echo "  test - Run the tests"
    exit 1
}


function lint {
    
    mkdir -p ${PROJECT_DIR}/.cache/pip
    mkdir -p ${PROJECT_DIR}/.cache/venv

    $DOCKER run --rm \
        -v "${PROJECT_DIR}/.cache/pip:/root/.cache/pip" \
        -v "${PROJECT_DIR}/.cache/venv:/venv/" \
        -v "$(git rev-parse --show-toplevel):/src" \
        -w "/src" \
        -ti \
        $BASE_IMAGE \
        /src/ci/lint-python.sh
    

}

function unit_test {

    mkdir -p ${PROJECT_DIR}/.cache/pip
    mkdir -p ${PROJECT_DIR}/.cache/venv

    $DOCKER run --rm \
        -v "${PROJECT_DIR}/.cache/pip:/root/.cache/pip" \
        -v "${PROJECT_DIR}/.cache/venv:/venv/" \
        -v "$(git rev-parse --show-toplevel):/src" \
        -w "/src" \
        -ti \
        $BASE_IMAGE \
        /src/ci/unit-test.sh
    

}

function functional_test {

    mkdir -p ${PROJECT_DIR}/.cache/pip
    mkdir -p ${PROJECT_DIR}/.cache/venv

    $DOCKER run --rm \
        -v "${PROJECT_DIR}/.cache/pip:/root/.cache/pip" \
        -v "${PROJECT_DIR}/.cache/venv:/venv/" \
        -v "$(git rev-parse --show-toplevel):/src" \
        -w "/src" \
        -ti \
        --cap-add SYS_ADMIN \
        --privileged=true \
        $BASE_IMAGE \
        /src/ci/functional-test.sh
    

}

function shell {

    mkdir -p ${PROJECT_DIR}/.cache/pip
    mkdir -p ${PROJECT_DIR}/.cache/venv

    $DOCKER run --rm \
        -v "${PROJECT_DIR}/.cache/pip:/root/.cache/pip" \
        -v "${PROJECT_DIR}/.cache/venv:/venv/" \
        -v "$(git rev-parse --show-toplevel):/src" \
        -w "/src" \
        -ti \
        --cap-add SYS_ADMIN \
        --privileged=true \
        $BASE_IMAGE \
        /src/ci/shell.sh
    

}


function build-deb {

    mkdir -p ${PROJECT_DIR}/.cache/pip
    mkdir -p ${PROJECT_DIR}/.cache/venv

    TARGET="${TARGET:-ubuntu:20.04}"
    echo "build for $TARGET"
    $DOCKER run --rm \
        -v "${PROJECT_DIR}/.cache/pip:/root/.cache/pip" \
        -v "${PROJECT_DIR}/.cache/venv:/venv/" \
        -v "$(git rev-parse --show-toplevel):/src" \
        -w "/src" \
        -ti \
        $TARGET \
        /src/ci/build-deb.sh
    

}

function test {
    unit_test
    functional_test
}


function main {
    [ $# -eq 1 ] && [[ "$1" =~ -h|--help ]] && usage
  
  # Run the given commands/functions
  [ $# -gt 0 ] && _run_commands "$@"

  usage

}

# Run the given commands/functions
function _run_commands {
  local retval=0
  for arg in "$@"; do
    $arg || local retval=1
  done
  exit $retval 
}

main "$@"