
function create_config {
    system_disk=$(mktemp /tmp/system.XXX.disk)
    dd of=${system_disk} bs=1 count=0 seek=512M
    mkfs.xfs ${system_disk}
    cat << EOF > /etc/flexfs.conf
filesystems:
    - name: test
      system_pool: ${system_disk}
EOF

}

function start_server {
    flexfsd > /var/log/flexfsd.log 2>&1 &
}

function mount_fs {
    mkdir -p /mnt/test
    DEBUG_FLAG=""
    if [[ -n "$DEBUG" ]]; then
        DEBUG_FLAG="-d"
    fi
    mount.flexfs ${DEBUG_FLAG} test /mnt/test > /var/log/flexfs.log 2>&1 &
}