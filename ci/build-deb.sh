#!/bin/bash

set -e 

# exec 3>&1
exec 3>/dev/null

export DEBIAN_FRONTEND=noninteractive
source /etc/os-release

if [[ "$VERSION_CODENAME" == "focal" ]]; then
    # dh-virtualenv isn't available on ubuntu 20:04 by default.
    # Add a suitable PPA
    apt-get update
    apt-get install -y software-properties-common
    add-apt-repository -y ppa:jyrki-pulliainen/dh-virtualenv
fi

apt-get install -y dpkg-dev
apt-get install \
    --yes $(dpkg-checkbuilddeps 2>&1 | sed -e 's/dpkg-checkbuilddeps:\serror:\sUnmet build dependencies: //g' -e  's/[\(][^)]*[\)] //g')
dpkg-buildpackage -b -uc -us

# Move the resulting deb to the dist dir and rename based on target OS.
DEB_VERSION=$(grep "^flexfs" debian/changelog | head -n 1 | sed 's!.*(\(.*\)).*!\1!')
FROMFILENAME=flexfs_${DEB_VERSION}_all.deb

VERSION_CODENAME="${VERSION_CODENAME:-unknown}"
DESTFILENAME=flexfs_${DEB_VERSION}.${VERSION_CODENAME}_all.deb
cp ../${FROMFILENAME} dist/${DESTFILENAME}

exit 0