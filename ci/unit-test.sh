#!/bin/bash

set -e 

VENV_DIR=/venv/

# exec 3>&1
exec 3>/dev/null




# General venv setup
mkdir -p ~/.cache/pip
chown root:root ~/.cache/pip
if [[ ! -f ${VENV_DIR}/bin/activate ]]; then
    python3 -m venv ${VENV_DIR}
fi
source ${VENV_DIR}/bin/activate
mkdir -p reports
pip install --upgrade pip >&3
pip install -r requirements-dev.txt  >&3
pip install -e .

pytest  --cov-report term-missing --cov=flexfs tests/unit
coverage xml -o reports/coverage.xml

exit 0