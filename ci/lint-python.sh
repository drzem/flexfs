#!/bin/bash

set -e 

VENV_DIR=/venv/


# General venv setup
mkdir -p ~/.cache/pip
chown root:root ~/.cache/pip

if [[ ! -f ${VENV_DIR}/bin/activate ]]; then
    python3 -m venv ${VENV_DIR}
fi
source ${VENV_DIR}/bin/activate
mkdir -p reports
pip install --upgrade pip >/dev/null
pip install -r requirements-dev.txt  >/dev/null
pip install -e .

flake8 flexfs

exit 0