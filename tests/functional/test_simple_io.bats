#!/usr/bin/env bats

setup() {
    load 'test_helper/bats-support/load'
    load 'test_helper/bats-assert/load'
    
}

export MOUNTPOINT="/mnt/test"
export SYSTEM_MOUNTPOINT="/var/lib/flexfs/mounts/test/system/"

gen_filename() {
    random_string=$(echo $RANDOM | md5sum | head -c 4)
    echo "test-${random_string}.file"
}

@test "write a file" {
    FILENAME=$(gen_filename)
    OURPATH="${MOUNTPOINT}/${FILENAME}"
    UNDERLYING_PATH="${SYSTEM_MOUNTPOINT}/${FILENAME}"

    run dd if=/dev/urandom of=$OURPATH bs=1k count=1
    assert_success

    run stat $OURPATH
    assert_success
    stat1=$OUTPUT
    run stat $UNDERLYING_PATH
    assert_success
    stat2=$OUTPUT
    assert [ "$stat1" == "$stat2" ]

}

function write_string_to_file {
   echo "$1" > "$2"
}

@test "read a file" {
    BODY="hello world"
    FILENAME="$(gen_filename)"
    OURPATH="${MOUNTPOINT}/${FILENAME}"
    UNDERLYING_PATH="${SYSTEM_MOUNTPOINT}/${FILENAME}"

    run write_string_to_file "$BODY" "$OURPATH"
    assert_success


    run cat $OURPATH
    assert_output "$BODY"
    run cat $UNDERLYING_PATH
    assert_output "$BODY"
}

# @test "chown a file" {
#     BODY="hello world"
#     FILENAME="$(gen_filename)"
#     OURPATH="${MOUNTPOINT}/${FILENAME}"
#     UNDERLYING_PATH="${SYSTEM_MOUNTPOINT}/${FILENAME}"

#     run write_string_to_file "$BODY" "$OURPATH"
#     assert_success

#     run chown 1:2 $OURPATH
#     assert_success
#     run stat --format="%u:%g" $OURPATH
#     assert_output "1:2"
#     run stat --format="%u:%g" $UNDERLYING_PATH
#     assert_output "1:2"
# }

@test "umask 022" {
    FILENAME="$(gen_filename)"
    OURPATH="${MOUNTPOINT}/${FILENAME}" 

    umask 022
    run write_string_to_file "$BODY" "$OURPATH"
    run stat --format="%a" $OURPATH
    assert_output "644"
}


@test "umask 000" {
    FILENAME="$(gen_filename)"
    OURPATH="${MOUNTPOINT}/${FILENAME}" 

    umask 000
    run write_string_to_file "$BODY" "$OURPATH"
    run stat --format="%a" $OURPATH
    assert_output "666"
}


@test "umask 002" {
    FILENAME="$(gen_filename)"
    OURPATH="${MOUNTPOINT}/${FILENAME}" 

    umask 002
    run write_string_to_file "$BODY" "$OURPATH"
    run stat --format="%a" $OURPATH
    assert_output "664"

}

@test "chmod a file" {
    BODY="hello world"
    FILENAME="$(gen_filename)"
    OURPATH="${MOUNTPOINT}/${FILENAME}"
    UNDERLYING_PATH="${SYSTEM_MOUNTPOINT}/${FILENAME}"

    run write_string_to_file "$BODY" "$OURPATH"
    assert_success

    run chmod 600 $OURPATH
    assert_success
    run stat --format="%a" $OURPATH
    assert_output "600"
    run stat --format="%a" $UNDERLYING_PATH
    assert_output "600"
}

@test "abosolute symlink" {
    BODY="hello world"
    FILENAME="$(gen_filename)"
    OURPATH="${MOUNTPOINT}/${FILENAME}"
    UNDERLYING_PATH="${SYSTEM_MOUNTPOINT}/${FILENAME}"

    run write_string_to_file "$BODY" "$OURPATH"
    assert_success

    run ln -s $OURPATH $OURPATH.symlink
    assert_success
    run stat $OURPATH.symlink --format=%F
    assert_output "symbolic link"
    run stat $OURPATH.symlink --format=%N
    assert_output "'${OURPATH}.symlink' -> '${OURPATH}'"

    run stat $UNDERLYING_PATH.symlink --format=%F
    assert_output "symbolic link"
    run stat $UNDERLYING_PATH.symlink --format=%N
    assert_output "'${UNDERLYING_PATH}.symlink' -> '${OURPATH}'"

}

@test "realtive symlink" {
    BODY="hello world"
    FILENAME="$(gen_filename)"
    OURPATH="${MOUNTPOINT}/${FILENAME}"
    UNDERLYING_PATH="${SYSTEM_MOUNTPOINT}/${FILENAME}"

    run write_string_to_file "$BODY" "$OURPATH"
    assert_success

    run ln -s ${FILENAME} $OURPATH.symlink
    assert_success
    run stat $OURPATH.symlink --format=%F
    assert_output "symbolic link"
    run stat $OURPATH.symlink --format=%N
    assert_output "'${OURPATH}.symlink' -> '${FILENAME}'"

    run stat $UNDERLYING_PATH.symlink --format=%F
    assert_output "symbolic link"
    run stat $UNDERLYING_PATH.symlink --format=%N
    assert_output "'${UNDERLYING_PATH}.symlink' -> '${FILENAME}'"

}

@test "hardlink" {
    BODY="hello world"
    FILENAME="$(gen_filename)"
    OURPATH="${MOUNTPOINT}/${FILENAME}"
    UNDERLYING_PATH="${SYSTEM_MOUNTPOINT}/${FILENAME}"

    run write_string_to_file "$BODY" "$OURPATH"
    assert_success

    run ln  $OURPATH $OURPATH.link
    assert_success
    run stat $OURPATH.link --format=%i
    inode1=$OUTPUT
    run stat $OURPATH --format=%i
    inode2=$OUTPUT
    assert [ "$inode1" == "$inode2" ]

    run stat $UNDERLYING_PATH --format=%i
    inode1=$OUTPUT
    run stat $UNDERLYING_PATH.link --format=%i
    inode2=$OUTPUT
    assert [ "$inode1" == "$inode2" ]

}

@test "mkdir" {
   
    NAME="$(gen_filename)"
    OURPATH="${MOUNTPOINT}/${NAME}"
    UNDERLYING_PATH="${SYSTEM_MOUNTPOINT}/${NAME}"

    run mkdir $OURPATH
    assert_success

    run stat $OURPATH --format=%F
    assert_output "directory"
    run stat $UNDERLYING_PATH --format=%F
    assert_output "directory"

}

@test "mknod" {
   
    NAME="$(gen_filename)"
    OURPATH="${MOUNTPOINT}/${NAME}"
    UNDERLYING_PATH="${SYSTEM_MOUNTPOINT}/${NAME}"

    run mknod  $OURPATH p
    assert_success

    run stat $OURPATH --format=%F
    assert_output "fifo"
    run stat $UNDERLYING_PATH --format=%F
    assert_output "fifo"

}