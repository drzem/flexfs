#!/usr/bin/env bats

setup() {
    load 'test_helper/bats-support/load'
    load 'test_helper/bats-assert/load'
}

get_system_mountpoint() {
    mount | grep "/var/lib/flexfs/mounts/test/system"
}

@test "Check the filesystem is mounted" {
    run mount -t fuse.flexfs
    assert_output "test on /mnt/test type fuse.flexfs (rw,nosuid,nodev,relatime,user_id=0,group_id=0,default_permissions,allow_other)"
  
}

@test "Test the system pool is mounted" {
    run get_system_mountpoint
    assert_output --regexp '^.*on /var/lib/flexfs/mounts/test/system type ext4 \(rw,relatime\)$'

}