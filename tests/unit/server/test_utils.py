import mock

from flexfs.server.utils import check_running_root, setup_logging


class TestCheckRunningRoot:

    def test_running_as_root(self):
        with mock.patch('os.getuid', return_value=0):
            assert check_running_root() is True

    def test_running_as_not_root(self):
        with mock.patch('os.getuid', return_value=1000):
            assert check_running_root() is False


class TestSetupLogging:

    def test_debug_enabled(self):
        setup_logging(debug=True)
