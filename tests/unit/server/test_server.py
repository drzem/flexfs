from click.testing import CliRunner
import mock
from mock.mock import MagicMock
import pytest
import json

from flexfs.server.server import start, Server
from flexfs.server.config import Config


class TestStart:

    def test_start_simple(self):
        runner = CliRunner()
        with mock.patch('flexfs.server.server.Server'):
            result = runner.invoke(start)
        assert result.exit_code == 0

    @mock.patch('flexfs.server.server.check_running_root')
    def test_start_not_root(self, mock_check_running_root):
        """
        Checks that we exist if not running as root
        """
        mock_check_running_root.return_value = False
        runner = CliRunner()
        result = runner.invoke(start)
        assert result.exit_code == 1


class TestServer:

    @mock.patch('zmq.Context.instance', mock.MagicMock)
    def test_server_simple_start(self):
        config = {
            "filesystems": [
                {
                    "name": "testfs",
                    "system_pool": "mock_block0"
                }
            ]
        }
        Server.main_loop = mock.MagicMock()
        Server(config)


class TestServerProcessMessage:

    @pytest.mark.timeout(5)
    @mock.patch('zmq.Context.instance', mock.MagicMock)
    def test_process_message_no_command(self):
        """
        Tests that an error is returned when no command is present in the payload
        """
        config = {
            "filesystems": [
                {
                    "name": "testfs",
                    "system_pool": "mock_block0"
                }
            ]
        }

        Server.main_loop = mock.MagicMock()
        server = Server(config)
        server.socket = mock.MagicMock()
        server.socket.recv_json.return_value = {"foo": "bar"}

        server.process_message()

        server.socket.send_json.assert_called_with({
            "status": "error",
            "message": "Received message with no command",
        })

    @pytest.mark.timeout(5)
    @mock.patch('zmq.Context.instance', mock.MagicMock)
    @pytest.mark.parametrize("fsname", ["fs1", "hippo"])
    def test_process_message_mount(self, fsname):
        """
        Tests that mount is called when the mount command is given
        """
        config = {
            "filesystems": [
                {
                    "name": "testfs",
                    "system_pool": "mock_block0"
                }
            ]
        }

        Server.main_loop = mock.MagicMock()
        server = Server(config)
        server.socket = mock.MagicMock()
        server.mount = mock.MagicMock()
        server.socket.recv_json.return_value = {"command": "mount", "fsname": fsname}

        server.process_message()

        server.mount.assert_called_with(fsname)

    @pytest.mark.timeout(5)
    @mock.patch('zmq.Context.instance', mock.MagicMock)
    @pytest.mark.parametrize("fsname", ["fs1", "hippo"])
    def test_process_message_mount_error(self, fsname):
        """
        Tests that mount is called when the mount command is given
        """
        config = {
            "filesystems": [
                {
                    "name": "testfs",
                    "system_pool": "mock_block0"
                }
            ]
        }

        Server.main_loop = mock.MagicMock()
        server = Server(config)
        server.socket = mock.MagicMock()
        server.mount = mock.MagicMock(side_effect=RuntimeError("unknown"))
        server.socket.recv_json.return_value = {"command": "mount", "fsname": fsname}

        server.process_message()

        server.mount.assert_called_with(fsname)
        server.socket.send_json.assert_called_with({
            "command": "mount",
            "status": "error",
            "message": "unknown"
        })

    @pytest.mark.timeout(5)
    @mock.patch('zmq.Context.instance', mock.MagicMock)
    def test_process_message_unknown_command(self):
        """
        Tests that an error is returned when an unknown command is provided
        """
        config = {
            "filesystems": [
                {
                    "name": "testfs",
                    "system_pool": "mock_block0"
                }
            ]
        }

        Server.main_loop = mock.MagicMock()
        server = Server(config)
        server.socket = mock.MagicMock()
        server.socket.recv_json.return_value = {"command": "hippo"}

        server.process_message()

        server.socket.send_json.assert_called_with({
            "command": "command",
            "status": "error",
            "message": "Received unexpected command: hippo",
        })


class TestMount:

    @pytest.mark.timeout(5)
    @mock.patch('zmq.Context.instance', mock.MagicMock)
    @mock.patch('os.path.exists')
    def test_mount_system_pool_not_exist(self, mock_path_exists):
        """
        Checks that the correct error is sent if the
        system pool device does not exist
        """
        config = {
            "filesystems": [
                {
                    "name": "testfs",
                    "system_pool": "mock_block0"
                }
            ]
        }

        mock_path_exists.return_value = False

        Server.main_loop = mock.MagicMock()
        server = Server(config)
        server.socket = mock.MagicMock()
        server.mount("testfs")

        mock_path_exists.assert_called_with("/dev/mock_block0")

        server.socket.send_json.assert_called_with({
            "command": "mount",
            "status": "error",
            "message": "system pool for testfs does not exists at /dev/mock_block0",
        })

    @pytest.mark.timeout(5)
    @mock.patch('zmq.Context.instance', mock.MagicMock)
    @mock.patch('os.path.exists')
    def test_mount_fs_not_defined(self, mock_path_exists):
        """
        Checks that the correct error is sent if the
        filesystem isn't defined
        """
        config = {
            "filesystems": [
                {
                    "name": "testfs",
                    "system_pool": "mock_block0"
                }
            ]
        }

        mock_path_exists.return_value = False

        Server.main_loop = mock.MagicMock()
        server = Server(config)
        server.socket = mock.MagicMock()
        server.mount("invalid")

        server.socket.send_json.assert_called_with({
            "command": "mount",
            "status": "error",
            "message": "invalid is not defined",
        })

    @pytest.mark.timeout(5)
    @mock.patch('zmq.Context.instance', mock.MagicMock)
    @mock.patch('os.path.exists')
    def test_mount_simple_ext4(self, mock_path_exists, fp):
        """
        Test a successful mount
        """
        system_pool_name = "mock_block0"
        fsname = "testfs"
        config = {
            "filesystems": [
                {
                    "name": fsname,
                    "system_pool": "mock_block0"
                }
            ]
        }

        fp.register(
            ["blkid", "-s", "TYPE", "-o", "value", f"/dev/{system_pool_name}"],
            stdout=["ext4"]
        )
        fp.register(
            ["/bin/findmnt", "-fnlJ", f"/dev/{system_pool_name}"],
            stdout=[""],
            returncode=1
        )
        fp.register(
            [
                "mount", "-t", "ext4", "-o", "defaults,user_xattr",
                f"/dev/{system_pool_name}", f"/var/lib/flexfs/mounts/{fsname}/system"
            ]
        )

        Server.main_loop = mock.MagicMock()
        server = Server(config)
        server.socket = mock.MagicMock()
        server.mount("testfs")

        mock_path_exists.assert_has_calls([
            mock.call('/dev/mock_block0'),
            mock.call().__bool__()
        ])
        server.socket.send_json.assert_called_with({
            "command": "mount",
            "status": "ok",
            "system_mountpoint": "/var/lib/flexfs/mounts/testfs/system"
        })

    @pytest.mark.timeout(5)
    @mock.patch('zmq.Context.instance', mock.MagicMock)
    @mock.patch('os.path.exists')
    def test_mount_simple_unknownfs(self, mock_path_exists, fp):
        """
        Test a successful mount
        """
        system_pool_name = "mock_block0"
        fsname = "testfs"
        config = {
            "filesystems": [
                {
                    "name": "testfs",
                    "system_pool": system_pool_name
                }
            ]
        }

        mock_path_exists.return_value = True
        fp.register(
            ["blkid", "-s", "TYPE", "-o", "value", f"/dev/{system_pool_name}"],
            stdout=["unknownfs"]
        )
        fp.register(
            ["/bin/findmnt", "-fnlJ", f"/dev/{system_pool_name}"],
            stdout=[""],
            returncode=1
        )
        fp.register(
            [
                "mount", "-t", "unknownfs", "-o", "defaults",
                f"/dev/{system_pool_name}", f"/var/lib/flexfs/mounts/{fsname}/system"
            ]
        )

        Server.main_loop = mock.MagicMock()
        server = Server(config)
        server.socket = mock.MagicMock()
        server.mount("testfs")

        mock_path_exists.assert_called_once()
        server.socket.send_json.assert_called_with({
            "command": "mount",
            "status": "ok",
            "system_mountpoint": "/var/lib/flexfs/mounts/testfs/system"
        })

    @pytest.mark.timeout(5)
    @mock.patch('zmq.Context.instance', mock.MagicMock)
    @mock.patch('os.path.exists')
    def test_mount_simple_ext4_fails(self, mock_path_exists, fp):
        """
        Test a failed mount due to the system pool already being mounted in an unexpected
        location.
        """
        system_pool_name = "mock_block0"
        fsname = "testfs"
        config = {
            "filesystems": [
                {
                    "name": fsname,
                    "system_pool": "mock_block0"
                }
            ]
        }

        mock_path_exists.return_value = True
        fp.register(
            ["blkid", "-s", "TYPE", "-o", "value", f"/dev/{system_pool_name}"],
            stdout=["ext4"]
        )
        fp.register(
            ["/bin/findmnt", "-fnlJ", f"/dev/{system_pool_name}"],
            stdout=json.dumps({
                "filesystems": [
                    {
                        "target": "/mnt/already_mounted_here",
                        "source": "/dev/{system_pool_name}", "fstype": "ext4", "options": "rw,relatime"
                    }
                ]
            }),
            returncode=0
        )

        # mock_check_output.side_effect = [
        #     b"ext4",  # return from blkid
        # ]
        # mock_run.return_value.returncode = 0
        # mock_run.return_value.output = json.dumps({
        #     "filesystems": [
        #         {
        #             "target": "/mnt/already_mounted_here",
        #             "source": "/dev/mock_block0", "fstype": "ext4", "options": "rw,relatime"
        #         }
        #     ]
        # }).encode('utf-8')

        Server.main_loop = mock.MagicMock()
        server = Server(config)
        server.socket = mock.MagicMock()
        server.mount("testfs")

        mock_path_exists.assert_called_once()
        server.socket.send_json.assert_called_with({
            "command": "mount",
            "status": "error",
            "message": "/dev/mock_block0 is unexpectedly mounted at /mnt/already_mounted_here"
        })
