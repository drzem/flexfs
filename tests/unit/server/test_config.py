import tempfile
import shutil

from flexfs.server.config import Config


class TestConfig:

    def test_save(self):
        basedir = tempfile.TemporaryDirectory()
        config = Config(filename=f"{basedir.name}/flexfs.conf")
        config.save()

    def test_save_no_existing_dir(self):
        basedir = tempfile.TemporaryDirectory()
        config = Config(filename=f"{basedir.name}/newdir/flexfs.conf")
        shutil.rmtree(f"{basedir.name}/newdir")
        config.save()
