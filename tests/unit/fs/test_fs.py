from tkinter import E
from flexfs.fs.fs import FlexFS

import pytest
import mock
import pyfuse3
import errno
import os


class TestFlexFS:

    @pytest.mark.asyncio
    @mock.patch('flexfs.fs.fs.FlexFS._getattr')
    async def test_lookup(self, mock_getattr):
        fs = FlexFS("/var/system")
        mock_getattr.return_value.st_ino = 2
        result = await fs.lookup(1, "test")
        assert result.st_ino == 2

    @pytest.mark.asyncio
    async def test_inode_to_path_not_exist(self):
        """
        Tests that looking up an inode that doesn't map
        to a path raises ENOENT
        """
        fs = FlexFS("/var/system")
        with pytest.raises(pyfuse3.FUSEError) as excinfo:
            fs._inode_to_path(7)
        assert excinfo.value.args[0] == errno.ENOENT

    @pytest.mark.asyncio
    async def test_inode_to_path_hardlink(self):
        """
        Tests that looking up an inode that is mapped to multiple paths 
        (e.g. hardlinks) returns the first path
        """
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = {'foo', 'bar'}
        result = fs._inode_to_path(7)
        assert result in {'foo', 'bar'}

    @pytest.mark.asyncio
    async def test_add_path_hardlink(self):
        """
        Test when adding a path to an inode with
        only one current link, the map is converted to set
        """
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = 'foo'
        fs._add_path(7, 'bar')
        assert fs._inode_path_map[7] == {"foo", "bar"}, fs._inode_path_map

    @pytest.mark.asyncio
    async def test_add_path_hardlink_multiple(self):
        """
        Test when adding a path to an inode with
        more than  one current link, the map is appended to the current
        set
        """
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = {'foo', 'bar'}
        fs._add_path(7, 'baz')
        assert fs._inode_path_map[7] == {"foo", "bar", "baz"}, fs._inode_path_map


class TestFSForget:

    @pytest.mark.asyncio
    async def test_forget_simple(self):
        """
        Tests a simple situation for forget:
        Where the lookup count is 1 and we're removing 1
        """
        fs = FlexFS("/var/system")
        fs._lookup_cnt[7] = 1
        fs._inode_path_map[7] = 'foo'
        await fs.forget([(7, 1)])
        assert 7 not in fs._lookup_cnt.keys(), fs._lookup_cnt
        assert 7 not in fs._inode_path_map.keys(), fs._inode_path_map

    @pytest.mark.asyncio
    async def test_forget_remaining_lookups(self):
        """
        Tests when issusing a forget, where the kernel still has remaining
        lookups, that we keep track and still have the map
        """
        fs = FlexFS("/var/system")
        fs._lookup_cnt[7] = 5
        fs._inode_path_map[7] = 'foo'
        await fs.forget([(7, 2)])
        assert 7 in fs._lookup_cnt.keys(), fs._lookup_cnt
        assert fs._lookup_cnt[7] == 3
        assert 7 in fs._inode_path_map.keys(), fs._inode_path_map

    @pytest.mark.asyncio
    async def test_forget_path_already_missing(self):
        """
        Tests when issusing a forget, when the path map has already been deleted
        that it behaves
        """
        fs = FlexFS("/var/system")
        fs._lookup_cnt[7] = 1
        await fs.forget([(7, 1)])
        assert 7 not in fs._lookup_cnt.keys(), fs._lookup_cnt
        assert 7 not in fs._inode_path_map.keys(), fs._inode_path_map


class TestGetAttr:

    @pytest.mark.asyncio
    @mock.patch('os.fstat')
    async def test_getattr_via_fd_map(self, mock_fstat):
        """
        Tests calling getattr when the inode has an open fd
        """
        fs = FlexFS("/var/system")
        fs._inode_fd_map[7] = 88
        await fs.getattr(7)
        mock_fstat.assert_called_with(88)

    @pytest.mark.asyncio
    @mock.patch('os.lstat')
    async def test_getattr_via_path_map(self, mock_lstat):
        """
        Tests calling getattr when the inode has an open fd
        """
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        await fs.getattr(7)
        mock_lstat.assert_called_with("/var/system/foo")

    @pytest.mark.asyncio
    @mock.patch('os.lstat')
    async def test_getattr_error(self, mock_lstat):
        """
        Tests calling getattr, we throw the correct exception
        """
        err = OSError("an_error")
        err.errno = 1
        mock_lstat.side_effect = err
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = 'foo'
        with pytest.raises(pyfuse3.FUSEError) as excinfo:
            await fs.getattr(7)
        assert excinfo.value.errno == 1


class TestReadlink:

    @mock.patch('os.readlink')
    async def test_readlink(self, mock_readlink):
        """
        Tests a successful readlink
        """
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        await fs.readlink(7, None)
        mock_readlink.assert_called_with("/var/system/foo")

    @mock.patch('os.readlink')
    async def test_readlink_failure(self, mock_readlink):
        """
        Tests we throw the correct error when readlink fails
        """
        err = OSError("an_error")
        err.errno = 1
        mock_readlink.side_effect = err

        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        with pytest.raises(pyfuse3.FUSEError) as excinfo:
            await fs.readlink(7, None)
        mock_readlink.assert_called_with("/var/system/foo")
        assert excinfo.value.errno == 1


class TestOpendir:

    async def test_opendir(self):
        """
        Tests the opendir call
        """
        fs = FlexFS("/var/system")
        result = await fs.opendir(7, None)
        assert result == 7


class TestReaddir:

    @mock.patch.object(FlexFS, '_getattr')
    @mock.patch('os.listdir')
    @mock.patch('pyfuse3.readdir_reply')
    async def test_readdir(self, mock_readdir_reply, mock_listdir, mock_getattr):
        """
        Test a successful readdir
        """
        entries = ['a', 'b', 'c']
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        mock_listdir.return_value = entries

        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        eattr.st_mode = 0o0644
        eattr.st_nlink = 1
        eattr.st_uid = 1000
        eattr.st_gid = 1000
        eattr.st_rdev = 123
        eattr.st_size = 1024
        eattr.st_atime_ns = 100
        eattr.st_mtime_ns = 100
        eattr.st_ctime_ns = 50
        mock_getattr.return_value = eattr
        await fs.readdir(7, 0, None)
        calls = []
        for entry in entries:
            calls.append(mock.call(None, os.fsencode(entry), eattr, eattr.st_ino))
            calls.append(mock.call().__bool__())
        mock_readdir_reply.assert_has_calls(calls)

    @mock.patch.object(FlexFS, '_getattr')
    @mock.patch('os.listdir')
    @mock.patch('pyfuse3.readdir_reply')
    async def test_readdir_with_special(self, mock_readdir_reply, mock_listdir, mock_getattr):
        """
        Test a readdir where somehow the listdir returns '.' and '..'
        Although the docs for os.listdir say this should never happen,
        the pyfuse3 examples catered for this, so we better make sure.
        """
        expected_entries = ['a', 'b', 'c']
        entries = expected_entries + ['.', '..']
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        mock_listdir.return_value = entries

        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        eattr.st_mode = 0o0644
        eattr.st_nlink = 1
        eattr.st_uid = 1000
        eattr.st_gid = 1000
        eattr.st_rdev = 123
        eattr.st_size = 1024
        eattr.st_atime_ns = 100
        eattr.st_mtime_ns = 100
        eattr.st_ctime_ns = 50
        mock_getattr.return_value = eattr
        await fs.readdir(7, 0, None)
        calls = []
        for entry in expected_entries:
            calls.append(mock.call(None, os.fsencode(entry), eattr, eattr.st_ino))
            calls.append(mock.call().__bool__())
        mock_readdir_reply.assert_has_calls(calls)

    @mock.patch.object(FlexFS, '_getattr')
    @mock.patch('os.listdir')
    @mock.patch('pyfuse3.readdir_reply')
    async def test_readdir_with_offset(self, mock_readdir_reply, mock_listdir, mock_getattr):
        """
        Test a readdir where somehow offset is >0
        """
        entries = ['a', 'b', 'c']
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        mock_listdir.return_value = entries

        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        eattr.st_mode = 0o0644
        eattr.st_nlink = 1
        eattr.st_uid = 1000
        eattr.st_gid = 1000
        eattr.st_rdev = 123
        eattr.st_size = 1024
        eattr.st_atime_ns = 100
        eattr.st_mtime_ns = 100
        eattr.st_ctime_ns = 50
        mock_getattr.return_value = eattr
        await fs.readdir(7, 9, None)
        calls = []
        mock_readdir_reply.assert_has_calls(calls)

    @mock.patch.object(FlexFS, '_getattr')
    @mock.patch('os.listdir')
    @mock.patch('pyfuse3.readdir_reply')
    async def test_readdir_no_continue(self, mock_readdir_reply, mock_listdir, mock_getattr):
        """
        Test a readdir where readdir_reply returns zero and check we don't continue

        from docs: If readdir_reply returns False, the lookup count must not be increased and the method should return without further calls to readdir_reply.
        """
        entries = ['a', 'b', 'c']
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        mock_listdir.return_value = entries

        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        eattr.st_mode = 0o0644
        eattr.st_nlink = 1
        eattr.st_uid = 1000
        eattr.st_gid = 1000
        eattr.st_rdev = 123
        eattr.st_size = 1024
        eattr.st_atime_ns = 100
        eattr.st_mtime_ns = 100
        eattr.st_ctime_ns = 50
        mock_getattr.return_value = eattr
        mock_readdir_reply.return_value = False
        await fs.readdir(7, 0, None)
        calls = []
        mock_readdir_reply.assert_has_calls(calls)


class TestUnlink:

    @mock.patch('os.unlink')
    @mock.patch('os.lstat')
    async def test_unlink_simple(self, mock_lstat, mock_unlink):
        """
        Test a successful unlink
        """
        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        mock_lstat.return_value = eattr
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        fs._inode_path_map[8] = '/foo/bar'
        fs._lookup_cnt[8] = 1
        await fs.unlink(7, 'bar', None)
        mock_unlink.assert_called_with('/var/system/foo/bar')

    @mock.patch('os.unlink')
    @mock.patch('os.lstat')
    async def test_unlink_with_hardlinks(self, mock_lstat, mock_unlink):
        """
        Test a successful unlink when there's a multiple links (hardlinks)
        """
        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        mock_lstat.return_value = eattr
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        fs._inode_path_map[8] = {'/foo/bar', '/foo/baz'}
        fs._lookup_cnt[8] = 1
        await fs.unlink(7, 'bar', None)
        mock_unlink.assert_called_with('/var/system/foo/bar')
        assert fs._inode_path_map[8] == '/foo/baz'

    @mock.patch('os.unlink')
    @mock.patch('os.lstat')
    async def test_unlink_with_error(self, mock_lstat, mock_unlink):
        """
        Tests when an unlink fails, what we throw the correct error
        """
        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        mock_lstat.return_value = eattr

        err = OSError("an_error")
        err.errno = 1
        mock_unlink.side_effect = err

        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        fs._inode_path_map[8] = '/foo/bar'
        fs._lookup_cnt[8] = 1
        with pytest.raises(pyfuse3.FUSEError) as excinfo:
            await fs.unlink(7, 'bar', None)
        assert excinfo.value.errno == 1
        mock_unlink.assert_called_with('/var/system/foo/bar')


class TestRmdir:

    @mock.patch('os.rmdir')
    @mock.patch('os.lstat')
    async def test_rmdir_simple(self, mock_lstat, mock_rmdir):
        """
        Test a successful rmdir
        """
        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        mock_lstat.return_value = eattr
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        fs._inode_path_map[8] = '/foo/bar'
        fs._lookup_cnt[8] = 1
        await fs.rmdir(7, 'bar', None)
        mock_rmdir.assert_called_with('/var/system/foo/bar')

    @mock.patch('os.rmdir')
    @mock.patch('os.lstat')
    async def test_rmdir_with_error(self, mock_lstat, mock_rmdir):
        """
        Tests when an rmdir fails, what we throw the correct error
        """
        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        mock_lstat.return_value = eattr

        err = OSError("an_error")
        err.errno = 1
        mock_rmdir.side_effect = err

        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        fs._inode_path_map[8] = '/foo/bar'
        fs._lookup_cnt[8] = 1
        with pytest.raises(pyfuse3.FUSEError) as excinfo:
            await fs.rmdir(7, 'bar', None)
        assert excinfo.value.errno == 1
        mock_rmdir.assert_called_with('/var/system/foo/bar')


class TestSymlink:

    @mock.patch('os.symlink')
    @mock.patch('os.chown')
    @mock.patch('os.lstat')
    async def test_symlink_simple(self, mock_lstat, mock_chown, mock_symlink):
        """
        Tests successfully making a symlink
        """
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        fs._inode_path_map[8] = '/foo/bar'
        fs._lookup_cnt[8] = 1
        ctx = mock.MagicMock()
        ctx.uid = 1000
        ctx.gid = 1001
        await fs.symlink(7, 'baz', '/foo/bar', ctx)
        mock_lstat.assert_called_with('/var/system/foo/baz')
        mock_chown.assert_called_with('/var/system/foo/baz', 1000, 1001, follow_symlinks=False)
        mock_symlink.assert_called_with('/foo/bar', '/var/system/foo/baz')

    @mock.patch('os.symlink')
    @mock.patch('os.chown')
    @mock.patch('os.lstat')
    async def test_symlink_failure(self, mock_lstat, mock_chown, mock_symlink):
        """
        Tests when a symlink fails, we throw the correct error
        """
        err = OSError("an_error")
        err.errno = 1
        mock_symlink.side_effect = err

        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        fs._inode_path_map[8] = '/foo/bar'
        fs._lookup_cnt[8] = 1
        ctx = mock.MagicMock()
        ctx.uid = 1000
        ctx.gid = 1001

        with pytest.raises(pyfuse3.FUSEError) as excinfo:
            await fs.symlink(7, 'baz', '/foo/bar', ctx)
        assert excinfo.value.errno == 1

        mock_symlink.assert_called_with('/foo/bar', '/var/system/foo/baz')


class TestRename:

    @mock.patch('os.lstat')
    @mock.patch('os.rename')
    async def test_rename_simple(self, mock_rename, mock_lstat):
        """
        Tests successfully renaming
        """
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        fs._lookup_cnt[7] = 1
        fs._inode_path_map[8] = '/foo/foo'
        fs._lookup_cnt[8] = 1
        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        mock_lstat.return_value = eattr

        await fs.rename(7, 'foo', 7, 'bar', 0, None)
        mock_rename.assert_called_with('/var/system/foo/foo', '/var/system/foo/bar')
        assert fs._inode_path_map[8] == '/foo/bar'

    async def test_rename_invalid_flag(self):
        """
        Tests that giving a non-zero flag throws an error
        """
        fs = FlexFS("/var/system")
        with pytest.raises(pyfuse3.FUSEError):
            await fs.rename(7, 'foo', 7, 'bar', 10, None)

    @mock.patch('os.lstat')
    @mock.patch('os.rename')
    async def test_rename_failure(self, mock_rename, mock_lstat):
        """
        Tests that if the underlying rename fails, we throw the correct error
        """
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        fs._lookup_cnt[7] = 1
        fs._inode_path_map[8] = '/foo/foo'
        fs._lookup_cnt[8] = 1
        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        mock_lstat.return_value = eattr
        err = OSError("an_error")
        err.errno = 1
        mock_rename.side_effect = err

        with pytest.raises(pyfuse3.FUSEError) as excinfo:
            await fs.rename(7, 'foo', 7, 'bar', 0, None)
        assert excinfo.value.errno == 1
        mock_rename.assert_called_with('/var/system/foo/foo', '/var/system/foo/bar')

    @mock.patch('os.lstat')
    @mock.patch('os.rename')
    async def test_rename_no_lookup(self, mock_rename, mock_lstat):
        """
        Tests successfully renaming, but skipping when there are currently no inode lookups cached
        """
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        fs._lookup_cnt[7] = 1
        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        mock_lstat.return_value = eattr

        await fs.rename(7, 'foo', 7, 'bar', 0, None)
        mock_rename.assert_called_with('/var/system/foo/foo', '/var/system/foo/bar')

    @mock.patch('os.lstat')
    @mock.patch('os.rename')
    async def test_rename_hardlinks(self, mock_rename, mock_lstat):
        """
        Tests successfully renaming when there are multiple links (hardlinks)
        """
        fs = FlexFS("/var/system")
        fs._inode_path_map[7] = '/foo'
        fs._lookup_cnt[7] = 1
        fs._inode_path_map[8] = {'/foo/foo', '/foo/foo2'}
        fs._lookup_cnt[8] = 1
        eattr = pyfuse3.EntryAttributes()
        eattr.st_ino = 8
        mock_lstat.return_value = eattr

        await fs.rename(7, 'foo', 7, 'bar', 0, None)
        mock_rename.assert_called_with('/var/system/foo/foo', '/var/system/foo/bar')
        assert fs._inode_path_map[8] == {'/foo/bar', '/foo/foo2'}
