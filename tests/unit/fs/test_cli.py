import mock
import pytest
from click.testing import CliRunner

from flexfs.fs.cli import mount


class TestMount:

    @mock.patch('pyfuse3.init', mock.MagicMock())
    @mock.patch('pyfuse3.close', mock.MagicMock())
    @mock.patch('trio.run', mock.MagicMock())
    @mock.patch('flexfs.fs.cli.get_zmq_socket.recv_json.get', mock.MagicMock(return_value="ok"))
    @mock.patch('flexfs.fs.cli.get_zmq_socket')
    def test_mount_simple(self, mock_zmq_socket):
        """
        Tests that the main mount function runs correctly under usual
        situations
        """
        mock_zmq_socket.return_value.recv_json.return_value.get.return_value = "ok"
        runner = CliRunner()
        result = runner.invoke(mount, ["-f", "testfs", "/mnt/testfs"])
        assert result.exit_code == 0, result.stdout

    @mock.patch('daemon.DaemonContext')
    @mock.patch('flexfs.fs.cli._mount')
    def test_mount_daemon(self, mock_mount, mock_daemon):
        """
        Tests that calling without -f will run as a daemon
        """
        runner = CliRunner()
        result = runner.invoke(mount, ["testfs", "/mnt/testfs"])
        assert result.exit_code == 0, result.stdout
        mock_daemon.assert_called_once()
        mock_mount.assert_called_with("testfs", "/mnt/testfs", None, False, True)

    @mock.patch('pyfuse3.init', mock.MagicMock())
    @mock.patch('pyfuse3.main', mock.MagicMock())
    @mock.patch('pyfuse3.close', mock.MagicMock())
    @mock.patch('trio.run', mock.MagicMock())
    @mock.patch('flexfs.fs.cli.get_zmq_socket.recv_json.get', mock.MagicMock(return_value="ok"))
    @mock.patch('flexfs.fs.cli.get_zmq_socket')
    def test_mount_server_unhealthy(self, mock_zmq_socket):
        """
        Tests that the main mount function errors correctly when the server is unhealthy
        """
        mock_zmq_socket.return_value.recv_json.return_value.get.return_value = "error"
        runner = CliRunner()
        result = runner.invoke(mount, ["-f", "testfs", "/mnt/testfs"])
        assert result.exit_code == 3, result.stdout
        assert result.stdout == "ERROR: error\n"

    @mock.patch('os.getuid', mock.MagicMock(return_value=1000))
    @mock.patch('pyfuse3.init', mock.MagicMock())
    @mock.patch('pyfuse3.main', mock.MagicMock())
    @mock.patch('pyfuse3.close', mock.MagicMock())
    @mock.patch('trio.run', mock.MagicMock())
    @mock.patch('flexfs.fs.cli.get_zmq_socket')
    def test_mount_not_root(self, mock_zmq_socket):
        """
        Tests the main mount function complains when not running as root
        """
        runner = CliRunner()
        result = runner.invoke(mount, ["-f", "testfs", "/mnt/testfs"])
        assert result.exit_code == 1, result.stdout
        assert result.stdout == "You must run this as root\n"

    @mock.patch('pyfuse3.init', mock.MagicMock())
    @mock.patch('pyfuse3.main', mock.MagicMock())
    @mock.patch('pyfuse3.close', mock.MagicMock())
    @mock.patch('trio.run', mock.MagicMock(side_effect=RuntimeError("unknown")))
    @mock.patch('flexfs.fs.cli.get_zmq_socket')
    def test_mount_other_runtime_error(self, mock_zmq_socket):
        """
        Tests that we still correctly raise unexpected runtime errors
        """
        mock_zmq_socket.return_value.recv_json.return_value.get.return_value = "ok"
        runner = CliRunner()
        with pytest.raises(RuntimeError):
            runner.invoke(mount, ["-f", "testfs", "/mnt/testfs"], catch_exceptions=False)
