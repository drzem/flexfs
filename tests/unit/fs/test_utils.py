import mock
import zmq

from flexfs.fs.utils import check_running_root, get_zmq_socket


class MockZmqSocket(mock.MagicMock):

    def __init__(self, **kwds):
        super().__init__(**kwds)
        self._connected = False

    def connect(self, endpoint):
        assert not self._connected
        assert endpoint != ""
        self._connected = True

    def close(self):
        assert self._connected
        self._connected = False


class MockZmqContext(mock.MagicMock):

    def __init__(self, **kwds):
        super().__init__(**kwds)
        self._destroyed = False
        self.IPV6 = None
        self.RCVTIMEO = None

    def socket(self, sock_type):
        assert not self._destroyed
        assert self.IPV6 == 1
        assert self.RCVTIMEO is not None
        assert sock_type == zmq.REQ
        return MockZmqSocket()

    def destroy(self, linger):
        assert not self._destroyed
        self._destroyed = True


class TestCheckRunningRoot:

    def test_running_as_root(self):
        with mock.patch('os.getuid', return_value=0):
            assert check_running_root() == True

    def test_running_as_not_root(self):
        with mock.patch('os.getuid', return_value=1000):
            assert check_running_root() == False


class TestGetZmqSocket:

    @mock.patch.object(zmq.Context, "instance", MockZmqContext)
    def test_get_zmq_socket(self):
        get_zmq_socket()
