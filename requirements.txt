#
# This file is autogenerated by pip-compile with python 3.8
# To update, run:
#
#    pip-compile setup.py
#
async-generator==1.10
    # via trio
attrs==21.4.0
    # via trio
click==8.0.4
    # via flexfs (setup.py)
docutils==0.18.1
    # via python-daemon
idna==3.3
    # via trio
lockfile==0.12.2
    # via python-daemon
outcome==1.1.0
    # via trio
pyfuse3==3.2.1
    # via flexfs (setup.py)
python-daemon==2.3.0
    # via flexfs (setup.py)
pyyaml==6.0
    # via flexfs (setup.py)
pyzmq==22.3.0
    # via flexfs (setup.py)
sniffio==1.2.0
    # via trio
sortedcontainers==2.4.0
    # via trio
trio==0.20.0
    # via
    #   flexfs (setup.py)
    #   pyfuse3

# The following packages are considered to be unsafe in a requirements file:
# setuptools
