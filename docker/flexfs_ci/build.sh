#!/bin/bash

set -e 

BASEDIR=$(dirname $0)

LABEL="danfoster/flexfs_ci"
TAG="latest"

docker build -t ${LABEL}:${TAG} $BASEDIR
docker push  ${LABEL}:${TAG}