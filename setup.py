import codecs
import os.path
from setuptools import setup, find_packages


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), 'r') as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith('__version__'):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


setup(
    name="flexfs",
    version=get_version("flexfs/__init__.py"),
    author="Dan Foster",
    author_email="dan@zem.org.uk",
    description="Flexible File System",
    license="Custom",
    packages=find_packages(),
    long_description="""Flexible File System""",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
    ],
    install_requires=[
        "click",
        "pyfuse3",
        "pyzmq",
        "trio",
        "pyyaml",
        "python-daemon"
    ],
    entry_points={
        'console_scripts': [
            'mount.flexfs=flexfs.fs.cli:mount',
            'flexfsd=flexfs.server.server:start',
        ],

    }
)
