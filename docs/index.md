# FlexFS

FlexFS is an advanced filesystem that manages your data across multiple storage pools, presenting it as a single namespace.

!!! warning
    Alpha Software - Use at your own risk. Please get in contact if you are interested in contributing.

```mermaid
graph LR
    A[FUSE] -->  B
    B["<br/><br/><br/><br/>FlexFS<br/><br/><br/><br/><br/>"] --> C
    B --> D
    B --> E
    C[System Pool] 
    D[Online Pools]
    E[Offline Pools]
```
    
The source can be found at: https://gitlab.com/drzem/flexfs