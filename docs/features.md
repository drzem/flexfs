# Features

* File hash checking
* Online pools for mixing disk types within a system
* Offline disk pools for cold storage on external HDDs
* Offline rclone pool for cold storage on cloud services