# Pool Types

A FlexFS filesystem is compromised of a number of pools:

## System Pool

Each filesystem requires one system pool. This is where primary data and metadata will be located.
The system pool needs to be a filesystem that supports extended attributes.
It is expected that the underlying devices are providing RAID protection.
It is recommended this pool is compromised of high IOP devices (SSD / nvme).

## Online Pool

Each filesystem can have zero or more online pools. Data located on an online pool can be accessed without being moved to the system pool.
It is expected that the underlying devices are providing RAID protection.


## Offline Pool

A number of different offline-pools exist:

### Offline Drive pool

Each filesystem can have zero or more offline drive pools.
Offline drives can be removed from the system.
Data is *not* striped across disks, therefore performance will be significantly lower, but provides greater flexibility for archiving.
FlexFS will mirror data across multiple disks for redundancy

### rclone pool

Each filesystem can have zer or more rclone pool.
A rclone pool can be any cloud storage that rclone supports (https://rclone.org/overview/).
Data located on a rclone pool will need to be recalled to the system pool before it is accessible.