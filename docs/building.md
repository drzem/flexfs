# Building

All the following sections assume you have a working docker environment and have a git clone of the source.

## Debian/Ubuntu/etc

The default target for building for ubuntu 20:04 can be run by:

```bash
ci/ci build-deb
```

If the build is successful, the resulting deb will be available at dist/`flexfs-version.codename_all.deb`. If the codename wasn't able to be detected, it will be set to `unknown`.

To build for a different distribution, set the `TARGET` environment variable to a suitable docker container. e.g:

```bash
TARGET=debian:bullseye ci/ci build-deb
```

