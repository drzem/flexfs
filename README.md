# FlexFS

FlexFS is an advanced filesystem that manages your data across multiple storage pools, presenting it as a single namespace.


**Warning**

Alpha Software - Use at your own risk. Please get in contact if you are interested in contributing.


Documentation can be found at <https://flexfs.readthedocs.io/>
