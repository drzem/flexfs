import os
import zmq

import logging
logger = logging.getLogger('flexfs.fs')


def check_running_root():
    """
    Checks we are running as root
    """
    if os.getuid() != 0:
        print("You must run this as root")
        return False
    return True


def get_zmq_socket():
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("ipc:///var/run/flexfs.sock")
    return socket


def setup_logging(debug=False, logfile=False):
    '''
    Configures logging
    '''
    formatter = logging.Formatter('[%(levelname)s] %(asctime)s - %(processName)s - %(message)s')
    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    if logfile:
        streamh = logging.FileHandler('/var/log/flexfs.log')
    else:
        streamh = logging.StreamHandler()

    streamh.setFormatter(formatter)
    logger.addHandler(streamh)
