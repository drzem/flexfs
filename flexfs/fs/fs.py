import pyfuse3
import os
import errno
from collections import defaultdict
import stat as stat_m
from os import fsencode, fsdecode

import logging
logger = logging.getLogger(__name__)


class FlexFS(pyfuse3.Operations):

    def __init__(self, system_mountpoint):
        super(FlexFS, self).__init__()
        logger.debug("init: %s", system_mountpoint)
        self.system_mountpoint = system_mountpoint
        self._inode_path_map = {pyfuse3.ROOT_INODE: '/'}
        self._lookup_cnt = defaultdict(lambda: 0)
        self._fd_inode_map = dict()
        self._inode_fd_map = dict()
        self._fd_open_count = dict()

    def _inode_to_path(self, inode):
        try:
            val = self._inode_path_map[inode]
        except KeyError:
            raise pyfuse3.FUSEError(errno.ENOENT)

        if isinstance(val, set):
            # In case of hardlinks, pick any path
            val = next(iter(val))
        return val

    def _add_path(self, inode, path):
        logger.debug('_add_path for %d, %s', inode, path)
        self._lookup_cnt[inode] += 1

        # With hardlinks, one inode may map to multiple paths.
        if inode not in self._inode_path_map:
            self._inode_path_map[inode] = path
            return

        val = self._inode_path_map[inode]
        if isinstance(val, set):
            val.add(path)
        elif val != path:
            self._inode_path_map[inode] = {path, val}

    async def forget(self, inode_list):
        for (inode, nlookup) in inode_list:
            if self._lookup_cnt[inode] > nlookup:
                self._lookup_cnt[inode] -= nlookup
                continue
            logger.debug('-> forget: inode %d', inode)
            assert inode not in self._inode_fd_map
            del self._lookup_cnt[inode]
            try:
                del self._inode_path_map[inode]
            except KeyError:  # may have been deleted
                pass

    async def lookup(self, inode_p, name, ctx=None):
        name = fsdecode(name)
        logger.debug('-> lookup: %s in %d', name, inode_p)
        path = os.path.join(self._inode_to_path(inode_p), name)
        realpath = self.system_mountpoint + path
        logger.debug('underlying: %s', realpath)
        attr = self._getattr(path=path)
        if name != '.' and name != '..':
            self._add_path(attr.st_ino, path)
        return attr

    async def getattr(self, inode, ctx=None):
        if inode in self._inode_fd_map:
            result = self._getattr(fd=self._inode_fd_map[inode])
        else:
            result = self._getattr(path=self._inode_to_path(inode))
        return result

    def _getattr(self, path=None, fd=None):
        assert fd is None or path is None
        assert not(fd is None and path is None)
        try:
            if fd is None:
                actualpath = self.system_mountpoint + path
                stat = os.lstat(actualpath)
            else:
                stat = os.fstat(fd)
        except OSError as exc:
            raise pyfuse3.FUSEError(exc.errno)

        entry = pyfuse3.EntryAttributes()
        for attr in ('st_ino', 'st_mode', 'st_nlink', 'st_uid', 'st_gid',
                     'st_rdev', 'st_size', 'st_atime_ns', 'st_mtime_ns',
                     'st_ctime_ns'):
            setattr(entry, attr, getattr(stat, attr))
        entry.generation = 0
        entry.entry_timeout = 0
        entry.attr_timeout = 0
        entry.st_blksize = 512
        entry.st_blocks = ((entry.st_size+entry.st_blksize-1) // entry.st_blksize)

        return entry

    async def readlink(self, inode, ctx):
        path = self._inode_to_path(inode)
        actualpath = self.system_mountpoint + path
        try:
            target = os.readlink(actualpath)
        except OSError as exc:
            raise pyfuse3.FUSEError(exc.errno)
        return fsencode(target)

    async def opendir(self, inode, ctx):
        return inode

    async def readdir(self, inode, off, token):
        path = self._inode_to_path(inode)
        actualpath = self.system_mountpoint + path
        logger.debug('-> readdir %s', path)
        entries = []
        for name in os.listdir(actualpath):
            if name == '.' or name == '..':
                continue
            attr = self._getattr(path=os.path.join(path, name))
            entries.append((attr.st_ino, name, attr))

        logger.debug('read %d entries, starting at %d', len(entries), off)

        # This is not fully posix compatible. If there are hardlinks
        # (two names with the same inode), we don't have a unique
        # offset to start in between them. Note that we cannot simply
        # count entries, because then we would skip over entries
        # (or return them more than once) if the number of directory
        # entries changes between two calls to readdir().
        for (ino, name, attr) in sorted(entries):
            if ino <= off:
                continue
            if not pyfuse3.readdir_reply(
                    token, fsencode(name), attr, ino):
                break
            self._add_path(attr.st_ino, os.path.join(path, name))

    async def unlink(self, inode_p, name, ctx):
        name = fsdecode(name)
        logger.debug("-> unlink: %s in %d", name, inode_p)
        parent = self._inode_to_path(inode_p)
        path = os.path.join(parent, name)
        actualpath = self.system_mountpoint + path
        try:
            inode = os.lstat(actualpath).st_ino
            os.unlink(actualpath)
        except OSError as exc:
            raise pyfuse3.FUSEError(exc.errno)
        if inode in self._lookup_cnt:
            self._forget_path(inode, path)

    async def rmdir(self, inode_p, name, ctx):
        name = fsdecode(name)
        parent = self._inode_to_path(inode_p)
        path = os.path.join(parent, name)
        actualpath = self.system_mountpoint + path
        try:
            inode = os.lstat(actualpath).st_ino
            os.rmdir(actualpath)
        except OSError as exc:
            raise pyfuse3.FUSEError(exc.errno)
        if inode in self._lookup_cnt:
            self._forget_path(inode, path)

    def _forget_path(self, inode, path):
        logger.debug('forget %s for %d', path, inode)
        val = self._inode_path_map[inode]
        if isinstance(val, set):
            # Handle hardlinks
            val.remove(path)
            if len(val) == 1:
                self._inode_path_map[inode] = next(iter(val))
        else:
            del self._inode_path_map[inode]

    async def symlink(self, inode_p, name, target, ctx):
        name = fsdecode(name)
        target = fsdecode(target)
        parent = self._inode_to_path(inode_p)
        path = os.path.join(parent, name)
        actualpath = self.system_mountpoint + path
        try:
            os.symlink(target, actualpath)
            os.chown(actualpath, ctx.uid, ctx.gid, follow_symlinks=False)
        except OSError as exc:
            logger.error("Exception in symlink")
            raise pyfuse3.FUSEError(exc.errno)
        stat = os.lstat(actualpath)
        self._add_path(stat.st_ino, path)
        return await self.getattr(stat.st_ino)

    async def rename(self, inode_p_old, name_old, inode_p_new, name_new,
                     flags, ctx):
        if flags != 0:
            logger.error("Exception in rename")
            raise pyfuse3.FUSEError(errno.EINVAL)

        name_old = fsdecode(name_old)
        name_new = fsdecode(name_new)
        parent_old = self._inode_to_path(inode_p_old)
        parent_new = self._inode_to_path(inode_p_new)
        path_old = os.path.join(parent_old, name_old)
        path_new = os.path.join(parent_new, name_new)
        actualpath_old = self.system_mountpoint + path_old
        actualpath_new = self.system_mountpoint + path_new
        try:
            os.rename(actualpath_old, actualpath_new)
            inode = os.lstat(actualpath_new).st_ino
        except OSError as exc:
            logger.error("Exception in rename (2)")
            raise pyfuse3.FUSEError(exc.errno)
        if inode not in self._lookup_cnt:
            return

        val = self._inode_path_map[inode]
        if isinstance(val, set):
            assert len(val) > 1
            val.add(path_new)
            val.remove(path_old)
        else:
            assert val == path_old
            self._inode_path_map[inode] = path_new

    async def link(self, inode, new_inode_p, new_name, ctx):
        new_name = fsdecode(new_name)
        parent = self._inode_to_path(new_inode_p)
        path = os.path.join(parent, new_name)
        actualpath_target = self.system_mountpoint + path
        try:
            actualpath_source = self.system_mountpoint + self._inode_to_path(inode)
            os.link(actualpath_source, actualpath_target, follow_symlinks=False)
        except OSError as exc:
            raise pyfuse3.FUSEError(exc.errno)
        self._add_path(inode, path)
        return await self.getattr(inode)

    async def setattr(self, inode, attr, fields, fh, ctx):
        # We use the f* functions if possible so that we can handle
        # a setattr() call for an inode without associated directory
        # handle.
        if fh is None:
            path_or_fh = self.system_mountpoint + self._inode_to_path(inode)
            truncate = os.truncate
            chmod = os.chmod
            chown = os.chown
            stat = os.lstat
        else:
            path_or_fh = fh
            truncate = os.ftruncate
            chmod = os.fchmod
            chown = os.fchown
            stat = os.fstat

        try:
            if fields.update_size:
                truncate(path_or_fh, attr.st_size)

            if fields.update_mode:
                # Under Linux, chmod always resolves symlinks so we should
                # actually never get a setattr() request for a symbolic
                # link.
                assert not stat_m.S_ISLNK(attr.st_mode)
                chmod(path_or_fh, stat_m.S_IMODE(attr.st_mode))

            if fields.update_uid:
                chown(path_or_fh, attr.st_uid, -1, follow_symlinks=False)

            if fields.update_gid:
                chown(path_or_fh, -1, attr.st_gid, follow_symlinks=False)

            if fields.update_atime and fields.update_mtime:
                if fh is None:
                    os.utime(path_or_fh, None, follow_symlinks=False,
                             ns=(attr.st_atime_ns, attr.st_mtime_ns))
                else:
                    os.utime(path_or_fh, None,
                             ns=(attr.st_atime_ns, attr.st_mtime_ns))
            elif fields.update_atime or fields.update_mtime:
                # We can only set both values, so we first need to retrieve the
                # one that we shouldn't be changing.
                oldstat = stat(path_or_fh)
                if not fields.update_atime:
                    attr.st_atime_ns = oldstat.st_atime_ns
                else:
                    attr.st_mtime_ns = oldstat.st_mtime_ns
                if fh is None:
                    os.utime(path_or_fh, None, follow_symlinks=False,
                             ns=(attr.st_atime_ns, attr.st_mtime_ns))
                else:
                    os.utime(path_or_fh, None,
                             ns=(attr.st_atime_ns, attr.st_mtime_ns))

        except OSError as exc:
            logger.error("Exception in setattr")
            raise pyfuse3.FUSEError(exc.errno)

        return await self.getattr(inode)

    async def mknod(self, inode_p, name, mode, rdev, ctx):
        logger.debug('-> mknod: %s in %d', name, inode_p)
        path = os.path.join(self._inode_to_path(inode_p), fsdecode(name))
        actualpath = self.system_mountpoint + path
        try:
            os.mknod(actualpath, mode=(mode & ~ctx.umask), device=rdev)
            os.chown(actualpath, ctx.uid, ctx.gid)
        except OSError as exc:
            logger.error("Exception in mknod")
            raise pyfuse3.FUSEError(exc.errno)
        attr = self._getattr(path=path)
        self._add_path(attr.st_ino, path)
        return attr

    async def mkdir(self, inode_p, name, mode, ctx):
        path = os.path.join(self._inode_to_path(inode_p), fsdecode(name))
        actualpath = self.system_mountpoint + path
        try:
            os.mkdir(actualpath, mode=(mode & ~ctx.umask))
            os.chown(actualpath, ctx.uid, ctx.gid)
        except OSError as exc:
            logger.error("Exception in mkdir")
            raise pyfuse3.FUSEError(exc.errno)
        attr = self._getattr(path=path)
        self._add_path(attr.st_ino, path)
        return attr

    async def statfs(self, ctx):
        root = self.system_mountpoint
        stat_ = pyfuse3.StatvfsData()
        try:
            statfs = os.statvfs(root)
        except OSError as exc:
            logger.error("Exception in statfs")
            raise pyfuse3.FUSEError(exc.errno)
        for attr in ('f_bsize', 'f_frsize', 'f_blocks', 'f_bfree', 'f_bavail',
                     'f_files', 'f_ffree', 'f_favail'):
            setattr(stat_, attr, getattr(statfs, attr))
        stat_.f_namemax = statfs.f_namemax - (len(root)+1)
        return stat_

    async def open(self, inode, flags, ctx):
        logger.debug("-> open: %s", inode)
        if inode in self._inode_fd_map:
            fd = self._inode_fd_map[inode]
            self._fd_open_count[fd] += 1
            return pyfuse3.FileInfo(fh=fd)
        assert flags & os.O_CREAT == 0
        try:
            realpath = self.system_mountpoint + self._inode_to_path(inode)
            logger.debug("open: %s", realpath)
            fd = os.open(realpath, flags)
        except OSError as exc:
            raise pyfuse3.FUSEError(exc.errno)
        self._inode_fd_map[inode] = fd
        self._fd_inode_map[fd] = inode
        self._fd_open_count[fd] = 1
        return pyfuse3.FileInfo(fh=fd)

    async def create(self, inode_p, name, mode, flags, ctx):
        logger.debug("-> create: inode_p: %s, name: %s, mode: %s, flags: %s", inode_p, name, mode, flags)
        path = os.path.join(self._inode_to_path(inode_p), fsdecode(name))
        actualpath = self.system_mountpoint + path
        try:
            os.setegid(ctx.gid)
            os.seteuid(ctx.uid)
            prevumask = os.umask(ctx.umask)
            fd = os.open(actualpath, flags | os.O_CREAT | os.O_TRUNC, mode=mode, )
            os.setegid(0)
            os.seteuid(0)
            os.umask(prevumask)
        except OSError as exc:
            raise pyfuse3.FUSEError(exc.errno)
        attr = self._getattr(fd=fd)
        self._add_path(attr.st_ino, path)
        self._inode_fd_map[attr.st_ino] = fd
        self._fd_inode_map[fd] = attr.st_ino
        self._fd_open_count[fd] = 1
        return (pyfuse3.FileInfo(fh=fd), attr)

    async def read(self, fd, offset, length):
        os.lseek(fd, offset, os.SEEK_SET)
        return os.read(fd, length)

    async def write(self, fd, offset, buf):
        os.lseek(fd, offset, os.SEEK_SET)
        return os.write(fd, buf)

    async def release(self, fd):
        if self._fd_open_count[fd] > 1:
            self._fd_open_count[fd] -= 1
            return

        del self._fd_open_count[fd]
        inode = self._fd_inode_map[fd]
        del self._inode_fd_map[inode]
        del self._fd_inode_map[fd]
        try:
            os.close(fd)
        except OSError as exc:
            raise pyfuse3.FUSEError(exc.errno)
