import click
import sys
import pyfuse3
import trio
import daemon

from .utils import check_running_root, get_zmq_socket
from .fs import FlexFS
from .utils import setup_logging


import logging
logger = logging.getLogger(__name__)


@click.command()
@click.argument("fsname")
@click.argument("mountpoint")
@click.option("-o", "--options")
@click.option("-d", "--debug", is_flag=True)
@click.option("-f", "--foreground", is_flag=True)
def mount(fsname, mountpoint, options, debug, foreground):
    if foreground:
        _mount(fsname, mountpoint, options, debug, False)
    else:
        with daemon.DaemonContext():
            _mount(fsname, mountpoint, options, debug, True)


def _mount(fsname, mountpoint, options, debug, logtofile):
    setup_logging(debug=debug, logfile=logtofile)

    if not check_running_root():
        sys.exit(1)

    socket = get_zmq_socket()
    socket.RCVTIMEO = 1000

    socket.send_json({
        "command": "mount",
        "fsname": fsname
    })
    response = socket.recv_json()

    if response.get("status") != "ok":
        print("ERROR: {}".format(response.get("message")))
        sys.exit(3)

    try:
        system_mountpoint = response.get("system_mountpoint")
        logger.debug("System pool at %s", system_mountpoint)
        fuse_options = set(pyfuse3.default_options)
        # Possible options here: https://github.com/libfuse/libfuse/blob/fuse-3.2.6/lib/mount.c#L80

        fuse_options.add('allow_other')
        fuse_options.add(f'fsname={fsname}')
        fuse_options.add('subtype=flexfs')
        flexfs = FlexFS(system_mountpoint)
        pyfuse3.init(flexfs, mountpoint, fuse_options)
        trio.run(pyfuse3.main)
    except Exception as e:
        pyfuse3.close(unmount=False)
        logger.error(e)
        raise

    pyfuse3.close()
