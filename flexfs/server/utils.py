import os
import logging

logger = logging.getLogger(__name__)


def check_running_root():
    """
    Checks we are running as root
    """
    if os.getuid() != 0:
        logger.error("You must run this as root")
        return False
    return True


def setup_logging(debug=False):
    parentLogger = logger.parent
    formatter = logging.Formatter('[%(levelname)s] %(asctime)s - %(message)s')
    if debug:
        parentLogger.setLevel(logging.DEBUG)
    else:
        parentLogger.setLevel(logging.INFO)

    streamh = logging.StreamHandler()
    streamh.setFormatter(formatter)
    parentLogger.addHandler(streamh)


def get_section_from_list(sections, keyname, value):
    for section in sections:
        if section[keyname] == value:
            return section
    return None
