import click
import logging
import sys
import zmq
import subprocess
import os
import pathlib
import json

from .config import Config
from .utils import check_running_root, setup_logging, get_section_from_list

logger = logging.getLogger(__name__)


class Server:

    def __init__(self, config):
        self.filesystem_processes = []
        self.config = config

        self.context = zmq.Context.instance()
        self.socket = self.context.socket(zmq.REP)
        self.socket.bind("ipc:///var/run/flexfs.sock")

        self.main_loop()

    def main_loop(self):  # pragma: no cover
        keep_running = True
        while keep_running:
            self.process_message()

    def process_message(self):
        message = self.socket.recv_json()
        command = message.get("command", None)
        if command is None:
            logger.error("Received message with no command")
            self.socket.send_json({
                "status": "error",
                "message": "Received message with no command",
            })
        elif command == "mount":
            fsname = message.get("fsname")
            try:
                self.mount(fsname)
            except Exception as err:
                logger.error(err)
                self.socket.send_json({
                    "command": "mount",
                    "status": "error",
                    "message": str(err)
                })
        else:
            logger.error(f"Received unexpected command: {command}")
            self.socket.send_json({
                "command": "command",
                "status": "error",
                "message": f"Received unexpected command: {command}",
            })

    def mount(self, fsname):
        logger.info(f"Received mount request for {fsname}")
        filesystems = self.config.get("filesystems")
        filesystem_config = get_section_from_list(filesystems, "name", fsname)

        # Check filesystem has config
        if not filesystem_config:
            logger.error(f"{fsname} is not defined")
            self.socket.send_json({
                "command": "mount",
                "status": "error",
                "message": f"{fsname} is not defined",
            })
            return

        # Get and mount the system pool
        system_pool_device_path = filesystem_config.get("system_pool")
        if not system_pool_device_path.startswith('/'):
            system_pool_device_path = f"/dev/{system_pool_device_path}"
        if not os.path.exists(system_pool_device_path):
            logger.error(
                f"system pool for {fsname} does not exists at {system_pool_device_path}")
            self.socket.send_json({
                "command": "mount",
                "status": "error",
                "message": f"system pool for {fsname} does not exists at {system_pool_device_path}",
            })
            return

        # Detect the fstype
        # TODO: Gracefully handle errors
        system_fstype = subprocess.check_output(["blkid", "-s", "TYPE",
                                                 "-o", "value", system_pool_device_path]).rstrip().decode('utf-8')

        # Set the correct
        mount_options = ["defaults"]
        if system_fstype == "ext4":
            mount_options.append("user_xattr")
        else:
            logger.warning(
                f"Untested filesystem type {system_fstype} for {system_fstype} systempool")

        # Mount the filesystem
        mountpoint = f"/var/lib/flexfs/mounts/{fsname}/system"

        # Check to see if it's already mounted

        run_result = subprocess.run(
            ["/bin/findmnt", "-fnlJ", system_pool_device_path],
            check=False, stdout=subprocess.PIPE
        )

        if run_result.returncode == 0:
            jsondata = json.loads(run_result.stdout.decode('utf-8'))
            existing_mountpoint = jsondata['filesystems'][0]['target']
            if existing_mountpoint != mountpoint:
                logger.error(
                    "%s is unexpectedly mounted at %s",
                    system_pool_device_path, existing_mountpoint
                )
                self.socket.send_json({
                    "command": "mount",
                    "status": "error",
                    "message": f"{system_pool_device_path} is unexpectedly mounted at {existing_mountpoint}",
                })
                return
        else:
            pathlib.Path(mountpoint).mkdir(parents=True, exist_ok=True)
            # TODO: Gracefully handle errors
            subprocess.check_call(
                [
                    "mount",
                    "-t", system_fstype,
                    "-o", ','.join(mount_options),
                    system_pool_device_path,
                    mountpoint
                ],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL
            )

        self.socket.send_json({
            "command": "mount",
            "status": "ok",
            "system_mountpoint": mountpoint,
        })


@click.command()
def start():
    """
    Start the server instance
    """
    setup_logging(False)
    logger.info("Starting server...")
    if not check_running_root():
        sys.exit(1)
    config = Config()
    Server(config)
