import yaml
import os
import pathlib
import logging

logger = logging.getLogger(__name__)


class Config:

    filename = os.path.expanduser("/etc/flexfs.conf")

    def __init__(self, filename="/etc/flexfs.conf"):
        self.filename = filename
        self.load(self.filename)
        self.set_defaults()

    def __del__(self):
        self.save()

    def load(self, filename):
        """
        Loads the configration from disk
        """

        if not os.path.exists(filename):
            dirname = os.path.dirname(filename)
            pathlib.Path(dirname).mkdir(parents=True, exist_ok=True)
            pathlib.Path(filename).touch()
            self.config = {}

        with open(filename) as file:
            self.config = yaml.load(file, Loader=yaml.FullLoader) or {}

    def save(self):
        """
        Saves the configuration to disk
        """
        if not os.path.exists(self.filename):
            dirname = os.path.dirname(self.filename)
            pathlib.Path(dirname).mkdir(parents=True, exist_ok=True)

        with open(self.filename, 'w') as file:
            yaml.dump(self.config, file)

    def get(self, key):
        return self.config[key]

    def set(self, key, value):
        self.config[key] = value

    def set_defaults(self):
        try:
            self.get("filesystems")
        except KeyError:
            logger.warning("No filesystems defined")
            self.set("filesystems", [])
